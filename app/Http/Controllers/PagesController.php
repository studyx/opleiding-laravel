<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function about(){
        //return 'About us';

        //$name = "Niels Vroman";

        $name = "Niels <span style='color:red;'>Vroman</span>";

        //return view('pages.about')->with('name', $name);

        /*return view('pages.about')->with([
            'first' => "Niels",
            'last' => 'Vroman'
        ]);*/

        //return view('pages.about', compact('name'));

        $persons = [
            'Niels Vroman',
            'Xavier Dekeyster'
        ];

        return view('pages.about', compact('name', 'persons'));

        // compact will look for variable with name
    }
}
