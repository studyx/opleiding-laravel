@extends('app')

@section('content')
<div class="content">
    <h1>About</h1>

    <p>Sed posuere consectetur est at lobortis. Nullam quis risus eget urna mollis ornare vel eu leo. Vestibulum id ligula porta felis euismod semper.</p>
</div>

@if($name == "Niels")
    <p>Naam is Niels.</p>
@else
    <p>Naam is niet Niels.</p>
@endif

@if(count('people'))
    @foreach($persons as $person)
        <p>{{ $person }}</p>
    @endforeach

@endif



@stop
