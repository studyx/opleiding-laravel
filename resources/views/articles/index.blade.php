@extends('app')

@section('content')
<div class="content">
    <h1>Articles</h1>

@if(count('articles'))
    @foreach($articles as $article)
        <a href="/articles/{{ $article->id }}">{{ $article->title }}</a>

        <!--<a href="{{ action('ArticlesController@show', [$article->id]) }}">{{ $article->title }}</a>-->

        <p>{{ $article->body }}</p>
    @endforeach

@endif

</div>

@stop
