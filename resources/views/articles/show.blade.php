@extends('app')

@section('content')
<div class="content">
    <h1>{{ $article->title }}</h1>
    <p>{{ $article->body }}</p>
</div>

@stop
